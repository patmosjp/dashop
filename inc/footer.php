<?
$add=ubicacion($link,'addr');
$ph=ubicacion($link,'ph');
$email=ubicacion($link,'email');
?>
<footer class="wrap-footer"> 
				<div class="colophon wigetized">
					<div class="container">
						<div class="row">
							<div class="col-md-3 col-sm-6 col-md-footer">
								<div class="widget widget_noo_infomation">
									<div class="noo-info-top">
										<a href="#">
											<img src="images/logo_footer.png" alt="Clip507">
										</a>
										<ul class="noo-infomation-attr">
											<li>
												<span class="icon-directions infomation-directions"></span>
												<address><?=$add?></address>
											</li>
											<li>
												<span class="icon-call-out infomation-icon"></span>
												<span> <?=$ph?></span>
											</li>
											<li>
												<span class="icon-envelope infomation-icon"></span>
												<span class="phone-text">
													<a href="mailto:<?=$email?>"><?=$email?></a>
												</span>
											</li>
										</ul>
									</div>
									<div class="social-all">
										<a href="#" class="fa fa-facebook"></a>
										<a href="#" class="fa fa-google"></a>
										<a href="#" class="fa fa-twitter"></a>
										<a href="#" class="fa fa-youtube"></a>
										<a href="#" class="fa fa-skype"></a>
									</div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6 col-md-footer">
								<div class="widget widget_nav_menu">
									<h4 class="widget-title">Clip507</h4>
									<ul class="menu">
										<li><a href="#">Conozca Clip507</a></li>
										<li><a href="#">Como comprar</a></li>
										<li><a href="#">Entrega de pedidos</a></li>
										<li><a href="#">Politicas de privacidad</a></li>
									</ul>
								</div>
							</div>
							<div class="col-md-3 col-sm-6 col-md-footer">
								<div class="widget widget_nav_menu">
									<h4 class="widget-title">Compradores</h4>
									<ul class="menu">
										<li><a href="#">Mi cuenta</a></li>
										<li><a href="#">Servicio al cliente</a></li>
										<li><a href="#">Quejas y Reclamos</a></li>
										<li><a href="#">Sugerencias de diseño</a></li>
									</ul>
								</div>
							</div>
							<div class="col-md-3 col-sm-6 col-md-footer">
								<div class="widget widget_noo_widget_instagram">
								<img src="images/miss.png" alt="Clip507">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="noo-bottom-bar-content">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<p class="copyright">Copyright 2016 Clip507</p>
							</div>
							<div class="col-md-6 text-right">
								<!--<a href="#">
									<img alt="payment" src="images/payment-cart.png"/>
								</a> -->
							</div>
						</div>
					</div>
				</div>
			</footer>