<?
session_start();
include("inc/connect.php");
include("inc/fuctions.php");

if (isset($_SESSION['us'])) {

$us=sessionuser($link,$_SESSION['us']);
$usuario=$us['name'];
$user=$_SESSION['us'];
}

$id=$_GET['id'];
$ip=$_GET['ip'];

//consultas
$catname=catname($link,$id,'cat','id');
$cv=catview($link);
$ct=cattag($link,$id);
$pc=prodinfo($link,$ip);
$rel=prodrel($link);
$relcat=prodrelcat($link,$catname,$ip);
$vbag=wievbag($link,$user);

//info producto
$p=mysql_fetch_array($pc);
$bag=checkbag($link,$p['cod'],$_SESSION['us']);


$img1=prodimagenlimit($link,$p['cod'],0);
$img2=prodimagenlimit($link,$p['cod'],1);
$img3=prodimagenlimit($link,$p['cod'],2);
if(!empty($img1)){$img1=$img1;}
if(!empty($img2)){$img2=$img2;}
if(!empty($img3)){$img3=$img3;}

?>
<!doctype html>
<html lang="en-US">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
		<link rel="shortcut icon" href="images/favicon.ico"/>
		<title>CLIP 507 | Su vida necesitas mas | T-Shirt Personalizados</title>
		
		<link rel='stylesheet' href='css/bootstrap.min.css' type='text/css' media='all' />
		<link rel='stylesheet' href='css/commerce.css' type='text/css' media='all'/>
		<link rel='stylesheet' href='css/prettyPhoto.css' type='text/css' media='all' />
		<link rel='stylesheet' href='css/owl.carousel.css' type='text/css' media='all' />
		<link rel='stylesheet' href='css/owl.theme.css' type='text/css' media='all' />
		<link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' media='all' />
		<link rel='stylesheet' href='css/simple-line-icons.css' type='text/css' media='all'/>
		<link href='http://fonts.googleapis.com/css?family=Raleway:100,300,400,500,600,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
		<link href="http://fonts.googleapis.com/css?family=Montserrat:700,400" rel="stylesheet" type="text/css">
		<link rel='stylesheet' href='css/style.css' type='text/css' media='all'/>
		<link rel='stylesheet' href='css/custom.css' type='text/css' media='all'/>
		<link rel="stylesheet" href='css/magnific-popup.css' type='text/css' media='all' />
		<link rel="stylesheet" href='css/preloader.css' type='text/css' media='all' />


		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

	</head>
	<body>
		<div id="loading">
			<div id="loading-center">
				<div id="loading-center-absolute">
					<div class="loader">Loading...</div>
				</div>
			</div> 
		</div>
		<div class="site">

			<? include ("inc/menuin.php") ?>

			<div class="content-area">
				<main id="main" class="site-main">
					<div class="container">
						<div class="row">
							<div class="noo-main col-md-9">
								<div class="commerce">
									<div class="noo-single-product">
										<div class="product">
											<div class="images">
												<div class="single-product-thumbail">
													<div class="item-img">
														<img width="70" height="70" src="img_data/<?=$img1?>" alt="" />
													</div>
													<div class="item-img">
														<img width="70" height="70" src="img_data/<?=$img2?>" alt="" />
													</div>
													<div class="item-img">
														<img width="70" height="70" src="img_data/<?=$img3?>" alt="" />
													</div>
												</div>
												<div class="single-product-slider">
													<div id="owl-carousel" class="owl-carousel">
														<div class="item">
															<a href="img_data/<?=$img1?>" data-rel="prettyPhoto[product-gallery]">
																<img width="870" height="1110" src="img_data/<?=$img1?>" alt="" /> 
															</a>
														</div>
														<div class="item">
															<a href="img_data/<?=$img2?>" data-rel="prettyPhoto[product-gallery]">
																<img width="870" height="1110" src="img_data/<?=$img2?>" alt="" /> 
															</a>
														</div>
														<div class="item">
															<a href="img_data/<?=$img3?>" data-rel="prettyPhoto[product-gallery]">
																<img width="870" height="1110" src="img_data/<?=$img3?>" alt="" /> 
															</a>
														</div>
											
													</div>
												</div>
											</div>
											<div class="summary entry-summary">
												<h1 class="product_title entry-title"><?=$p['nombre']?></h1>
												<p class="price">
													<span class="amount"><?=money_format('%i', $p['price'])?></span>
												</p>
												<p class="description">
													<?=$p['des']?>
												</p>
												<?if($bag==0){?>
													<a rel="nofollow" onclick="shopin(<?=$p['cod']?>,'prod_view.php?id=<?=$id?>&ip=<?=$p['id']?>',<?=$shop?>)" class="button add_to_cart_button">Agregar (+1)</a>
		                                                   	<?}?>
		                                        <?if($bag==1){?>
													<a rel="nofollow" href="bag.php" class="button add_to_cart_button">En mi Bolsa</a>
		                                        <?}?>
												
												<div class="clear"></div>
												<div class="product_meta">
													<span class="posted_in">
														Categoría: <a href="cat.php?id=<?=$id?>"><?=strtoupper($catname)?></a>
													</span>
													
												</div>
												<div class="noo-social-share">
													<span><i class="icon-share"></i>Compartir:</span>
													<a href="#share" class="noo-share">
														<i class="fa fa-facebook"></i>
													</a>
													<a href="#share" class="noo-share">
														<i class="fa fa-twitter"></i>
													</a>
								
												</div>
											</div> 
											<div class="clear"></div>
											

																	<div class="related products">
												<h2 class="title-related">Relacionados<span></span></h2>
												<div class="products row noo-product-grid">

<?while($rc=mysql_fetch_array($relcat)){ 

$img1=prodimagenlimit($link,$rc['cod'],0);
$img2=prodimagenlimit($link,$rc['cod'],1);

?>													
													<div class="noo-product-column col-md-4 col-sm-6">
														<div class="noo-product-item">
															<div class="noo-product-thumbnail">
																<a href="prod_view.php?id=<?=$id?>&ip=<?=$rc['id']?>">
																	<img width="420" height="535" src="img_data/<?=$img1?>" alt="" />
																	<img width="420" height="535" src="img_data/<?=$img2?>" class="second-img" alt="" /> 
																</a>
															</div>
															<div class="product-item-ds">
																<h3><center><a href="prod_view.php?id=<?=$id?>&ip=<?=$rc['id']?>"><?=$rc['nombre']?></a></center></h3>
																
																<span class="price">
																	<span class="amount"><center><?=money_format('%i', $rc['price'])?></center></span>
																</span>
																
															</div>
														</div>
													</div>
<?}?>
													
													</div>

												</div>

									

										</div>
									</div>
								</div>
							</div>

							<div class="noo-sidebar col-md-3">
								<div class="noo-sidebar-wrap">
									<div class="widget widget_product_categories">
										<h3 class="widget-title">Categorías</h3>
										<ul class="product-categories">
											<li><a href="#"><?=strtoupper($catname)?></a></li>
										</ul>
									</div>
									<div class="widget commerce widget_products">
										<h3 class="widget-title">Mas Productos</h3>
										<ul class="product_list_widget">
<?while($r=mysql_fetch_array($rel)){ 

$img1=prodimagenlimit($link,$r['cod'],0);
?>											
											<li>
												<a href="prod_view.php?id=<?=$id?>&ip=<?=$r['id']?>">
													<img width="100" height="100" src="img_data/<?=$img1?>" alt="<?=$r['nombre']?>" /> 
													<span class="product-title"><?=$r['nombre']?></span>
												</a>
												<span class="amount"><?=money_format('%i', $r['price'])?></span>
											</li>
<?}?>											
											
											
										</ul>
									</div>
									<div class="widget widget_product_tag_cloud">
										<h3 class="widget-title">Tags</h3>
										<div class="tagcloud">
										<? while($c=mysql_fetch_array($ct)){ ?>	
											<a href="cat.php?id=<?=$c['id']?>"><?=$c['cat']?></a>
										<?}?>	
						
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</main>
			</div>  

<? include("inc/footer.php") ?>

		</div>

		<a href="shop-detail.html#" class="go-to-top hidden-print"><i class="fa fa-angle-up"></i></a>

		<script type='text/javascript' src='js/jquery.min.js'></script>
		<script type='text/javascript' src='js/bootstrap.min.js'></script>
		<script type='text/javascript' src='js/jquery-migrate.min.js'></script>
		<script type='text/javascript' src='js/modernizr-2.7.1.min.js'></script>
		<script type='text/javascript' src='js/jquery.prettyPhoto.js'></script>
		<script type='text/javascript' src='js/jquery.prettyPhoto.init.min.js'></script>
		<script type='text/javascript' src='js/off-cavnass.js'></script>
		<script type='text/javascript' src='js/owl.carousel.min.js'></script>
		<script type='text/javascript' src='js/jquery.parallax-1.1.3.js'></script>
		<script type='text/javascript' src='js/jquery.plugin.min.js'></script>
		<script type='text/javascript' src='js/jquery.countdown.min.js'></script>
		<script type='text/javascript' src='js/script.js'></script>
		<script type='text/javascript' src='js/custom.js'></script>
		<script type='text/javascript' src='js/jquery.fitvids.js'></script>
		<script type='text/javascript' src='js/jquery.magnific-popup.js'></script>
		<script type='text/javascript' src='js/jflickrfeed.min.js'></script>
		<script type='text/javascript' src='js/jquery.cookie.js'></script>

    </body>
</html>