<?
session_start();
include("inc/connect.php");
include("inc/fuctions.php");

$cv=catview($link);

if (isset($_SESSION['us'])) {

$us=sessionuser($link,$_SESSION['us']);
$usuario=$us['name'];
$_SESSION['email']=$us['email'];
}else{
header ("Location: login.php");
}

?>
<!doctype html>
<html lang="en-US">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
		<link rel="shortcut icon" href="images/favicon.ico"/>
		<title>CLIP 507 | Su vida necesitas mas | T-Shirt Personalizados</title>
		
		<link rel='stylesheet' href='css/bootstrap.min.css' type='text/css' media='all'/>
		<link rel='stylesheet' href='css/commerce.css' type='text/css' media='all'/>
		<link rel='stylesheet' href='css/prettyPhoto.css' type='text/css' media='all' />
		<link rel='stylesheet' href='css/owl.carousel.css' type='text/css' media='all' />
		<link rel='stylesheet' href='css/owl.theme.css' type='text/css' media='all' />
		<link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' media='all' />
		<link rel='stylesheet' href='css/simple-line-icons.css' type='text/css' media='all'/>
		<link href='http://fonts.googleapis.com/css?family=Raleway:100,300,400,500,600,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
		<link href="http://fonts.googleapis.com/css?family=Montserrat:700,400" rel="stylesheet" type="text/css">
		<link rel='stylesheet' href='css/style.css' type='text/css' media='all'/>
		<link rel='stylesheet' href='css/custom.css' type='text/css' media='all'/>
		<link rel="stylesheet" href='css/magnific-popup.css' type='text/css' media='all' />
		<link rel="stylesheet" href='css/preloader.css' type='text/css' media='all' />


		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->



	</head>
	<body>
		<div id="loading">
			<div id="loading-center">
				<div id="loading-center-absolute">
					<div class="loader">Loading...</div>
				</div>
			</div> 
		</div>
		<div class="site">

			<? include ("inc/menuin.php") ?>

			<div class="content-area">
				<main id="main" class="site-main">
					<div class="container">
						<div class="row">
							<div class="commerce commerce-account">
								<div id="customer_login">
	                				
									<div class="col-md-6">
										<h2>Soy Cliente Clip507</h2>
										
										
										<form class="login" id="login">
									         <div class="form-row form-row-wide">
												<label for="username">Email  <span class="required">*</span></label>
													<input type="text" class="input-text" name="user" id="user" value="<?=$us['email']?>" disabled />
											</div>
											<div id="upass">
											<div class="form-row form-row-wide">
												<label for="reg_password">
													Contraseña 
													<span class="required">*</span>
												</label>
												<input type="password" class="input-text" name="pass" id="pass" />
											</div>
											<div class="form-row form-row-wide">
												<label for="reg_password">
													Confirmar Contraseña
													<span class="required">*</span>
												</label>
												<input type="password" class="input-text" name="passb" id="passb" />
											</div>
											</div>
											<div class="form-row">
											<input type="button" class="button" name="login" onclick="psupdate()" value="Actualizar Clave" />
											</div>
											
										</form>
										
									
									</div>
									<div class="col-md-6">
									<img  src="images/mp.png" alt="Clip507" />	
									</div>
								</div>
							</div>
						</div>
					</div>
				</main>
			</div>  
			
			<? include("inc/footer.php") ?>
		</div>

		<a href="my-account.html#" class="go-to-top hidden-print"><i class="fa fa-angle-up"></i></a>

		<script type='text/javascript' src='js/jquery.min.js'></script>
		<script type='text/javascript' src='js/bootstrap.min.js'></script>
		<script type='text/javascript' src='js/jquery-migrate.min.js'></script>
		<script type='text/javascript' src='js/modernizr-2.7.1.min.js'></script>
		<script type='text/javascript' src='js/jquery.prettyPhoto.js'></script>
		<script type='text/javascript' src='js/jquery.prettyPhoto.init.min.js'></script>
		<script type='text/javascript' src='js/off-cavnass.js'></script>
		<script type='text/javascript' src='js/owl.carousel.min.js'></script>
		<script type='text/javascript' src='js/jquery.parallax-1.1.3.js'></script>
		<script type='text/javascript' src='js/jquery.plugin.min.js'></script>
		<script type='text/javascript' src='js/jquery.countdown.min.js'></script>
		<script type='text/javascript' src='js/script.js'></script>
		<script type='text/javascript' src='js/custom.js'></script>
		<script type='text/javascript' src='js/jquery.magnific-popup.js'></script>
		<script type='text/javascript' src='js/jflickrfeed.min.js'></script>
		<script type='text/javascript' src='js/jquery.cookie.js'></script>

    </body>
</html>