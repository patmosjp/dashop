<?
session_start();
include("../inc/connect.php");
include("../inc/fuctions.php");

if (isset($_SESSION['us'])) {
$us=webuser($link,$_SESSION['us']);

$ped=wievbagpanel($link);
$vbag=wievbagprocess($link,$_GET['k'],$_GET['cod']);

}else{
header ("Location: index.php");
}



?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>Administrador Web Clip507</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<link href="bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>

<div id="wrapper">

<? include("nav.php") ?>
<? include("menu.php") ?>


  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Dashboard</h4>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      
      <!-- row -->
         <div class="row">
          <div class="col-md-12 col-xs-12">
            <div class="white-box">
              <ul class="nav nav-tabs tabs customtab">
                
                <li class="active tab"><a href="profile.php#home" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Detalle de Pedido Nº <?=$_GET['cod']?></span> </a></li>
                <li class="tab"><a href="pedidos.php"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Volver</span> </a></li>
                
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="home">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="white-box">
                                               
                        <table id="myTable" class="table table-striped">
                          <thead>
                            <tr>
                              <th>Producto</th>
                              <th>Nombre</th>
                              <th>Precio</th>
                              <th>Cantidad</th>
                              <th>Subtotal</th>
                              <th>&nbsp;</th>
                            </tr>
                          </thead>
                          <tbody>
<? while($b=mysql_fetch_array($vbag)){ 

$pi=proddetail ($link,$b['cod']);
$catid=catname($link,$pi['cat'],'id','cat');

$img1=prodimagenlimit($link,$pi['cod'],0);

$precio=$b['qty']*$b['price'];

?>
                            <tr>
                              <td><img width="100" height="100" src="../img_data/<?=$img1?>" alt="" /></td>
                              <td><?=$pi['nombre']?></td>
                              <td><?=money_format('%i', $pi['price'])?></td>
                              <td><?=$b['qty']?></td>
                              <td>USD <?=($b['qty']*$b['price'])?></td>
                              <td><i class="fa fa-check" aria-hidden="true"></i></td>
                            </tr>
<?}?>

                          </tbody>
                        </table>

                      </div>
                    </div>
                </div>
                  



                </div>



              </div>
            </div>
          </div>
        </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
    
<? include("footer.php") ?>

</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<script src="bower_components/datatables/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function(){
      $('#myTable').DataTable();
      $(document).ready(function() {
        var table = $('#example').DataTable({
          "columnDefs": [
          { "visible": false, "targets": 2 }
          ],
          "order": [[ 2, 'asc' ]],
          "displayLength": 25,
          "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;

            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
              if ( last !== group ) {
                $(rows).eq( i ).before(
                  '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                  );

                last = group;
              }
            } );
          }
        } );

    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
      var currentOrder = table.order()[0];
      if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
        table.order( [ 2, 'desc' ] ).draw();
      }
      else {
        table.order( [ 2, 'asc' ] ).draw();
      }
    } );
  } );
    });
  </script>

<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
</body>
</html>
