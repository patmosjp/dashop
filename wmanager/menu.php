  <div class="navbar-default sidebar nicescroll" role="navigation">
    <div class="sidebar-nav navbar-collapse ">
      <ul class="nav" id="side-menu">
        <li class="sidebar-search hidden-sm hidden-md hidden-lg">

          <!-- /input-group -->
        </li>
        <li class="nav-small-cap">Main Menu</li>
          <li> <a href="profile.php" class="waves-effect"><i class="glyphicon glyphicon-star fa-fw"></i> Dashboard</a> </li>
          <li> <a href="categoria.php" class="waves-effect"><i class="glyphicon glyphicon-star fa-fw"></i> Categorias</a> </li>
          <li> <a href="producto.php" class="waves-effect"><i class="glyphicon glyphicon-star fa-fw"></i> Productos</a> </li>
          <li> <a href="#" class="waves-effect"><i class="glyphicon glyphicon-star fa-fw"></i> Clientes<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                  <li> <a href="registro.php">Registro</a> </li>
                  <li><a href="pedidos.php">Pedidos</a></li>
                  <li><a href="contactenos.php">Contáctenos</a></li>
                </ul>
          </li>
          <li> <a href="#" class="waves-effect"><i class="glyphicon glyphicon-star fa-fw"></i> Web Maker<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                  <li> <a href="banner.php">Banner</a> </li>
                  <li><a href="contact.php">Contacto</a></li>
                </ul>
          </li>
          <li> <a href="blog.php" class="waves-effect"><i class="glyphicon glyphicon-star fa-fw"></i> Blog</a> </li>




      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>
