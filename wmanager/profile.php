<?
session_start();
include("../inc/connect.php");
include("../inc/fuctions.php");

if (isset($_SESSION['us'])) {
$us=webuser($link,$_SESSION['us']);
$pq=prodview($link);
$ped=wievbagpanel($link);



}else{
header ("Location: index.php");
}



?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
<title>Administrador Web Clip507</title>
<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<link href="bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>

<div id="wrapper">

<? include("nav.php") ?>
<? include("menu.php") ?>


  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Dashboard</h4>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      
      <!-- row -->
         <div class="row">
          <div class="col-md-12 col-xs-12">
            <div class="white-box">
              <ul class="nav nav-tabs tabs customtab">
                
                <li class="active tab"><a href="profile.php#home" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Catálogo</span> </a> </li>

                <li class="tab"><a href="profile.php#profile" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Pedidos</span> </a> </li>
                
                
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="home">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="white-box">
                                               
                        <table id="myTable" class="table table-striped">
                          <thead>
                            <tr>
                              <th>Foto</th>
                              <th>Pid</th>
                              <th>Categoria</th>
                              <th>Producto</th>
                              <th>Precio</th>
                            </tr>
                          </thead>
                          <tbody>
<? while($p=mysql_fetch_array($pq)){ 

$img=prodimagen($link,$p['cod']);
?>
                            <tr>
                              <td><img src="../img_data/<?=$img?>" alt="<?=$p['nombre']?>" height="63" width="50"></td>
                              <td><?=$p['cod']?></td>
                              <td><?=$p['cat']?></td>
                              <td><?=$p['nombre']?></td>
                              <td>USD <?=$p['price']?></td>
                            </tr>
<?}?>
                          </tbody>
                        </table>

                      </div>
                    </div>
                </div>
                  



                </div>

                <div class="tab-pane" id="profile">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="white-box">
                                               
                        <table id="myTable" class="table table-striped">
                          <thead>
                            <tr>
                              <th>Pedido</th>
                              <th>Nombre</th>
                              <th>Email</th>
                              <th>Telefono</th>
                              <th>Monto</th>
                              <th>Estado</th>
                            </tr>
                          </thead>
                          <tbody>
<? while($pd=mysql_fetch_array($ped)){ 

$us=sessionuser($link,$pd['keyuser']);
$estado=$pd['status'];
if($estado==2){$estado='<i class="fa fa-archive" aria-hidden="true"></i> Pendiente';}
if($estado==3){$estado='<i class="fa fa-archive" aria-hidden="true"></i> Entregado';}

?>
                            <tr>
                              <td><?=$pd['codshop']?></td>
                              <td><?=$us['name']?></td>
                              <td><?=$us['email']?></td>
                              <td><?=$us['phone']?></td>
                              <td>USD <?=$pd['mount']?></td>
                              <td><?=$estado?></td>
                            </tr>
<?}?>

                          </tbody>
                        </table>

                      </div>
                    </div>
                </div>
                 
                  
                </div>


              </div>
            </div>
          </div>
        </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
    
<? include("footer.php") ?>

</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="js/jquery.nicescroll.js"></script>
<script src="bower_components/datatables/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function(){
      $('#myTable').DataTable();
      $(document).ready(function() {
        var table = $('#example').DataTable({
          "columnDefs": [
          { "visible": false, "targets": 2 }
          ],
          "order": [[ 2, 'asc' ]],
          "displayLength": 25,
          "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;

            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
              if ( last !== group ) {
                $(rows).eq( i ).before(
                  '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                  );

                last = group;
              }
            } );
          }
        } );

    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
      var currentOrder = table.order()[0];
      if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
        table.order( [ 2, 'desc' ] ).draw();
      }
      else {
        table.order( [ 2, 'asc' ] ).draw();
      }
    } );
  } );
    });
  </script>

<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/myadmin.js"></script>
</body>
</html>
