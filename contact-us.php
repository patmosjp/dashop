<?
session_start();
include("inc/connect.php");
include("inc/fuctions.php");

if (isset($_SESSION['us'])) {

$us=sessionuser($link,$_SESSION['us']);
$usuario=$us['name'];
$user=$_SESSION['us'];
}	

$cv=catview($link);
$vbag=wievbag($link,$user);

?>
<!doctype html>
<html lang="en-US">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
		<link rel="shortcut icon" href="images/favicon.ico"/>
		<title>CLIP 507 | Su vida necesitas mas | T-Shirt Personalizados</title>
		
		<link rel='stylesheet' href='css/bootstrap.min.css' type='text/css' media='all'/>
		<link rel='stylesheet' href='css/commerce.css' type='text/css' media='all'/>
		<link rel='stylesheet' href='css/prettyPhoto.css' type='text/css' media='all' />
		<link rel='stylesheet' href='css/owl.carousel.css' type='text/css' media='all' />
		<link rel='stylesheet' href='css/owl.theme.css' type='text/css' media='all' />
		<link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' media='all' />
		<link rel='stylesheet' href='css/simple-line-icons.css' type='text/css' media='all'/>
		<link href='http://fonts.googleapis.com/css?family=Raleway:100,300,400,500,600,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
		<link href="http://fonts.googleapis.com/css?family=Montserrat:700,400" rel="stylesheet" type="text/css">
		<link rel='stylesheet' href='css/style.css' type='text/css' media='all'/>
		<link rel='stylesheet' href='css/custom.css' type='text/css' media='all'/>
		<link rel="stylesheet" href='css/magnific-popup.css' type='text/css' media='all' />
		<link rel="stylesheet" href='css/preloader.css' type='text/css' media='all' />


		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
	</head>
	<body>
		<div id="loading">
			<div id="loading-center">
				<div id="loading-center-absolute">
					<div class="loader">Loading...</div>
				</div>
			</div> 
		</div>
		<div class="site">

		<? include ("inc/menuin.php") ?>

			<div class="content-area">
				<div class="pt-0 pb-0">
					<center><img src="images/Space.png" alt="Clip507"></center>
				</div>
				<div class="pt-0 pb-0">
				<hr>	
				</div>	
<?if (isset($_SESSION['tk'])) {	?>
				<div class="pt-0 pb-0">
					<div class="container">
						<div class="row">
							<div class="col-sm-5">
								<h3 class="noo-attach">Gracias por escribirnos</h3> 
								<h2 class="noo-sh-title">¡Gracias!</h2>
								<div class="pt-3 pb-6">
									<p>
									Pronto te responderemos, hemos asignado a un agente de servicio al cliente que te contactara en poco tiempo.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
<?
session_destroy();
}?>				
<?if (!isset($_SESSION['tk'])) {	?>			
				<div class="pt-0 pb-0">
					<div class="container">
						<div class="row">
							<div class="col-sm-5">
								<h3 class="noo-attach">Deja un mensaje</h3> 
								<h2 class="noo-sh-title">Contáctenos</h2>
								<div class="pt-3 pb-6">
									<p>
										Asignaremos a un agente de servicio al cliente que te contactara en poco tiempo.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			
				<div class="pb-12">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<form class="contact-form">
									<div class="row">
										<div class="col-md-5">
											<div class="form-row">
												Nombre <em>*</em><br/>
												<div class="contact-form-control-wrap your-name">
													<input type="text" name="name" id="name" value="" size="40" class="contact-form-control" placeholder="¿Como se llama?"/>
												</div>
											</div>
											<div class="form-row">
												Email <em>*</em><br/>
												<div class="contact-form-control-wrap your-email">
													<input type="email" name="email" id="email" value="" size="40" class="contact-form-control" placeholder="¿A donde le enviamos info?" />
												</div>
											</div>
											<div class="form-row">
												Telefono<br/>
												<div class="contact-form-control-wrap your-subject">
													<input type="text" name="ph" id="ph" value="" size="40" class="contact-form-control" placeholder="¿A donde lo llamamos?"/>
												</div>
											</div>
											<div class="form-row">
												<input type="button" onclick="contact()" value="Contactar"  class="contact-form-control contact-submit"/>
											</div>
										</div>
										<div class="col-md-7">
											<div class="form-row">
												Mensaje<br/>
												<div class="contact-form-control-wrap your-message">
													<textarea name="sms" id="sms" cols="40" rows="10" class="contact-form-control contact-textarea" placeholder="¿Que dudas tiene?"></textarea>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
<?}?>				
				<div class="mt-10 mb-7">
					<div class="container">
						<div class="row">
							<div class="col-sm-4">
								<div class="noo-information">
									<i class="fa fa-phone"></i>
									<h5>Telefono</h5>
									<p>
										Phone 01: +(084) 888 - 6789

									</p>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="noo-information">
									<i class="fa fa-location-arrow"></i>
									<h5>Ubicación</h5>
									<p>1001 Milacian Crest Street, Behind Victoria, Paris France</p>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="noo-information">
									<i class="fa fa-envelope-o"></i>
									<h5>Email</h5>
									<p>
										<a href="mailto:email@domain.com">email@domain.com</a>
									</p>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>  

			<? include("inc/footer.php") ?>

		</div>

		<a href="contact-us.html#" class="go-to-top hidden-print"><i class="fa fa-angle-up"></i></a>

		<script type='text/javascript' src='js/jquery.min.js'></script>
		<script type='text/javascript' src='js/bootstrap.min.js'></script>
		<script type='text/javascript' src='js/jquery-migrate.min.js'></script>
		<script type='text/javascript' src='js/modernizr-2.7.1.min.js'></script>
		<script type='text/javascript' src='js/jquery.prettyPhoto.js'></script>
		<script type='text/javascript' src='js/jquery.prettyPhoto.init.min.js'></script>
		<script type='text/javascript' src='js/off-cavnass.js'></script>
		<script type='text/javascript' src='js/owl.carousel.min.js'></script>
		<script type='text/javascript' src='js/jquery.parallax-1.1.3.js'></script>
		<script type='text/javascript' src='js/jquery.plugin.min.js'></script>
		<script type='text/javascript' src='js/jquery.countdown.min.js'></script>
		<script type='text/javascript' src='js/script.js'></script>
		<script type='text/javascript' src='js/custom.js'></script>
		<script type='text/javascript' src='js/jquery.magnific-popup.js'></script>
		<script type='text/javascript' src='js/jflickrfeed.min.js'></script>
		<script type='text/javascript' src='js/jquery.cookie.js'></script>

    </body>
</html>